<h1 align="center">
Proffy - server
</h1>

<p align="center">API</p>

<p align="center">
  <a href="https://opensource.org/licenses/MIT">
    <img src="https://img.shields.io/badge/License-MIT-blue.svg" alt="License MIT">
  </a>
</p>

## Recursos

- ⚛️ **Type Script** — Permite tipagem no código
- ⚛️ **Express** — Framework web
- ⚛️ **Knex** — Query Buider
- ⚛️ **sqlite3** — Micro db

## Iniciando o projeto localmente
- instalando as dependências <br>
`yarn install` <br>
- iniciando as migrates <br>
`yarn knex:migrate`<br>
- iniciando o serviço <br>
`yarn start`


## License

This project is licensed under the MIT License - see the [LICENSE](https://opensource.org/licenses/MIT) page for details.
